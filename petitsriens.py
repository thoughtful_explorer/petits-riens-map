#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import simplekml
import geocoder
import re

#Set filename/path for KML file output
kmlfile = "petitsriens.kml"
#Set KML schema name
kmlschemaname = "petitsriens"
#Set page URL
pageURL = "https://petitsriens.be/boutiques/"

#Returns reusable BeautifulSoup of a given url (defaults to locations list page)
def getsoupinit(url=pageURL):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(url)
    #Return the soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns a list of urls, each associated with a different store location
def getstores():
    #Get soup of default URL
    soup = getsoupinit()
    #The funcion-rollover-title class is the most specific we can get to the urls we need, but returns triple repititions of the needed information, so leave the final 28 links by truncating the first 56
    storelinks = soup(class_="fusion-rollover-title")[56:]
    #Initialize empty store list
    storelist = []
    #Iterate through each store
    for store in storelinks:
        #Get the pure url from the href attribute
        url = store.a['href']
        #Append this url to the collector list
        storelist.append(url)
    #Return the list of urls
    return storelist

#Returns store information for a given url
def getstore(url):
    #Get soup of the given url
    soup = getsoupinit(url)
    #Get the name of the store from the page title
    storename = soup.title.get_text()
    #Get the address of the store...
    try: #..however, if this is not a normal store page...
        storeaddress = soup(class_="fusion-text")[0].strong.get_text()
    except AttributeError:
        try: #...because it has some non-address text where the address text usually appears and/or is oddly within a <p> tag...
            storeaddress = soup(class_="fusion-text")[0].p.strong.get_text()
        except: #... then turn instead to the second instance of fusion-text and look for <strong> text inside a <p> tag.
            storeaddress = soup(class_="fusion-text")[1].p.strong.get_text()
    #Run the geocode
    geo = geocoder.osm(storeaddress)
    #Get coordinates from the geocode
    lat = geo.y
    lng = geo.x
    #Return all the goods
    return storename, storeaddress, lat, lng

#Saves a KML file of a given list of stores
def createkml():
    #Initialize kml objct
    kml = simplekml.Kml()
    #Add schema, which is required for a custom address field
    schema = kml.newschema(name=kmlschemaname)
    schema.newsimplefield(name="address",type="string")
    #Look through all the store urls
    for store in getstores():
        #Get all the returned variables from the getstore() function
        storename, storeaddress, lat, lng = getstore(store)
        #First, create the point name and description in the kml
        point = kml.newpoint(name=storename,description="thrift store")
        #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
        point.extendeddata.schemadata.schemaurl = kmlschemaname
        simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)

createkml()
